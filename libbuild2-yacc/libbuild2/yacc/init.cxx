#include <libbuild2/yacc/init.hxx>

#include <libbuild2/yacc/rule.hxx>
#include <libbuild2/yacc/target.hxx>

#include <libbuild2/diagnostics.hxx>
#include <libbuild2/rule.hxx>

namespace build2
{
  namespace yacc
  {
    namespace
    {
      const compile_rule compile_rule_;

      bool
      init (scope& rs,
            scope& bs,
            const location& l,
            unique_ptr<module_base>&,
            const bool first,
            const bool,
            const variable_map&)
      {
        tracer trace{"yacc::init"};
        l2([&]{ trace << "for " << bs; });

        if (!cast_false<bool>(bs["c.loaded"]))
        {
          fail(l) << "c module must be loaded before yacc";
        }

// Register variables and target types.

        if (first)
        {
          auto& v{rs.ctx.var_pool.rw(rs)};
          v.insert<process_path>("yacc.path");

          const path yacc{"yacc"};
          rs.assign("yacc.path") = process::path_search(yacc,
                                                        /*init*/true,
                                                        dir_path(),
                                                        /*path_only*/true);

          rs.insert_target_type<y>();
          rs.insert_target_type<tab_c>();
        }

  // Register our rules.

        const std::tuple<meta_operation_id, operation_id> ops[]{
          {perform_id, update_id},
          {perform_id, clean_id},
          {configure_id, update_id},
          {dist_id, update_id}
        };
        for (const auto& op : ops)
        {
          bs.rules.insert<y>(std::get<0>(op),
                             std::get<1>(op),
                             "yacc.compile",
                             compile_rule_);
          bs.rules.insert<tab_c>(std::get<0>(op),
                                 std::get<1>(op),
                                 "yacc.compile",
                                 compile_rule_);
        }

        info(l) << "module yacc initialised";
        return true;
      }

      static const module_functions mod_functions[] =
      {
        {"yacc", nullptr, init},
        {nullptr, nullptr, nullptr}
      };
    }

    const module_functions*
    build2_yacc_load ()
    {
      return mod_functions;
    }
  }
}
