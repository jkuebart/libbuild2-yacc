#include <libbuild2/yacc/rule.hxx>

#include <libbuild2/yacc/target.hxx>

#include <libbuild2/algorithm.hxx>
#include <libbuild2/depdb.hxx>
#include <libbuild2/diagnostics.hxx>
#include <libbuild2/utility.hxx>

namespace build2
{
  namespace yacc
  {
    namespace
    {
      template<typename Prerequisites>
      std::optional<prerequisite_member>
      find(tracer& trace, const action a, const target& t, const Prerequisites& r)
      {
        for (prerequisite_member p : r)
        {
          if (include(a, t, p) != include_type::normal)
          {
            continue;
          }

          if (p.is_a<y>())
          {
            l2([&]{ trace << "match prerequisite is_a<y>"; });

            const std::size_t i{t.name.find(p.name())};
            if (std::string::npos != i)
            {
              return p;
            }
            l2([&]{ trace << ".y file stem '" << p.name() << "' "
                             "doesn't match target " << t;});
          }
        }
        return std::nullopt;
      }
    }

    bool
    compile_rule::match(const action a, target& t, const string&) const
    {
      tracer trace{"yacc::compile_rule::match"};
      l2([&]{ trace << "match(" << a << ", " << t << ")"; });

      if (tab_c* const c{t.is_a<tab_c>()})
      {
        l2([&]{ trace << "match is_a<tab_c>"; });

        if (!find(trace, a, t, group_prerequisite_members(a, *c)))
        {
          l2([&]{ trace << "no .y source file for target " << *c;});
          return false;
        }

        c->c = &search<c::c>(*c, c->dir, c->out, c->name);

        return true;
      }
      else
      {
        l2([&]{ trace << "match find<tab_c>"; });

// Check if there is a corresponding tab.c{} group.

        const tab_c* g{t.ctx.targets.find<tab_c>(t.dir, t.out, t.name)};

// If not or if it has no prerequisites (happens when we use it to set
// yacc.options) and this target has a y{} prerequisite, then synthesize
// the dependency.

        if (g == nullptr || !g->has_prerequisites())
        {
          if (const auto p{find(trace, a, t, prerequisite_members(a, t))})
          {
            if (g == nullptr)
            {
              g = &t.ctx.targets.insert<tab_c>(t.dir, t.out, t.name, trace);
            }

            g->prerequisites(prerequisites{p->as_prerequisite()});
          }
        }

        if (g == nullptr)
        {
          return false;
        }

        t.group = g;
        return true;
      }
    }

    recipe
    compile_rule::apply(const action a, target& xt) const
    {
      tracer trace{"yacc::compile_rule::apply"};
      l2([&]{ trace << "apply(" << a << ", " << xt << ")"; });

      if (tab_c* const c{xt.is_a<tab_c>()})
      {
        l2([&]{ trace << "apply is_a<tab_c>"; });
        c->c->derive_path();
        inject_fsdir(a, *c);
        match_prerequisite_members(a, *c);

        switch (a)
        {
        case perform_update_id: return &perform_update;
        case perform_clean_id: return &perform_clean_group_depdb;
        }
        return noop_recipe;
      }
      else
      {
        const tab_c& g{xt.group->as<tab_c>()};
        l2([&]{ trace << "apply group->as<tab_c>"; });
        build2::match(a, g);
        return group_recipe;
      }
    }

    target_state
    compile_rule::perform_update(action a, const target& xt)
    {
      tracer trace{"yacc::compile_rule::perform_update"};
      l2([&]{ trace << "perform_update(" << a << ", " << xt << ")"; });

// The rule has been matched which means the members should be resolved and
// paths assigned. We use the tab.c as our "target path" for timestamp,
// depdb, etc.

      const tab_c& t{xt.as<tab_c>()};
      const path& tp{t.c->path()};

// Update prerequisites and determine if any relevant ones render us
// out-of-date.

      const timestamp mt{t.load_mtime(tp)};
      const auto pr{execute_prerequisites<y>(a, t, mt)};

      bool update{!pr.first};
      const target_state ts{update ? target_state::changed : *pr.first};
      const y& s{pr.second};

// We use depdb to track changes to the .y file name, options, yacc
// version, etc.

      depdb dd{tp + ".d"};

      if (dd.expect("yacc.compile 1") != nullptr)
      {
        l2([&]{ trace << "rule mismatch forcing update of " << t; });
      }

      if (dd.expect(s.path()) != nullptr)
      {
        l2([&]{ trace << "input file mismatch forcing update of " << t; });
      }

      if (dd.writing() || dd.mtime > mt)
      {
        update = true;
      }

      dd.close();

      if (!update)
      {
        return ts;
      }

      const scope& rs{t.root_scope()};
      const process_path& yacc{cast<process_path>(rs["yacc.path"])};

      cstrings args{{
        yacc.recall_string(),
        "-b",
        "test",
        s.path().string().c_str(),
        nullptr
      }};

      if (2 <= verb)
      {
        print_process(args);
      }
      else
      {
        text << "yacc " << s;
      }

      if (!t.ctx.dry_run)
      {
        run(yacc, args);
      }

      t.mtime(system_clock::now());
      return target_state::changed;
    }
  }
}
