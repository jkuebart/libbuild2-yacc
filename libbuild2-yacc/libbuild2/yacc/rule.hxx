#ifndef LIBBUILD2_YACC_RULE_HXX
#define LIBBUILD2_YACC_RULE_HXX

#include <libbuild2/rule.hxx>

namespace build2
{
  namespace yacc
  {
    class compile_rule: public rule
    {
    public:
      bool
      match(action, target&, const string&) const override;

      recipe
      apply(action, target&) const override;

      static target_state
      perform_update(action, const target&);
    };
  }
}

#endif // LIBBUILD2_YACC_RULE_HXX
