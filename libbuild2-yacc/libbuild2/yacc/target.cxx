#include <libbuild2/yacc/target.hxx>

namespace build2
{
  namespace yacc
  {
    const char y_ext_def[]{"y"};

    const target_type y::static_type
    {
      "y",
      &file::static_type,
      &target_factory<y>,
      nullptr, /* fixed_extension */
      &target_extension_var<y_ext_def>,
      &target_pattern_var<y_ext_def>,
      nullptr,
      &file_search,
      false
    };

    group_view tab_c::
    group_members(action a) const
    {
      tracer trace{"yacc::tab_c::group_members"};
      l2([&]{ trace << "group_members(" << a << ")"; });
      return group_view{reinterpret_cast<const target* const*>(&c), 1};
    }

    static target*
    tab_c_factory(context& ctx, const target_type&, dir_path d, dir_path o, std::string n)
    {
      tracer trace{"yacc::tab_c_factory"};
      l2([&]{ trace << "for " << d; });

      ctx.targets.insert<c::c>(d, o, n, trace);

      return new tab_c{ctx, std::move(d), std::move(o), std::move(n)};
    }

    const target_type tab_c::static_type
    {
      "tab.c",
      &mtime_target::static_type,
      &tab_c_factory,
      nullptr,
      nullptr,
      nullptr,
      nullptr,
      &target_search,
      true
    };
  }
}
