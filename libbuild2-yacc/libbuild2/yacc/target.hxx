#ifndef BUILD2_YACC_TARGET_HXX
#define BUILD2_YACC_TARGET_HXX

#include <libbuild2/target.hxx>

#include <libbuild2/c/target.hxx>

namespace build2
{
  namespace yacc
  {
    class y: public file
    {
    public:
      using file::file;

    public:
      static const target_type static_type;
      const target_type& dynamic_type() const override
      {
        return static_type;
      }
    };

    class tab_c: public mtime_target
    {
    public:
      using mtime_target::mtime_target;

      group_view group_members(action) const override;

    public:
      static const target_type static_type;
      const target_type& dynamic_type() const override
      {
        return static_type;
      }

    public:
      const c::c* c{nullptr};
    };
  }
}

#endif // BUILD2_YACC_TARGET_HXX
